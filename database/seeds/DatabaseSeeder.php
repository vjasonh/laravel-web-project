<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('admin'),
            'image' => '',
            'gender' => '',
            'dob' => '',
            'address' => '',
            'admin' => '1'
        ]);

        DB::table('users')->insert([
            'name' => 'Jason',
            'email' => 'huangjason89@gmail.com',
            'password' => bcrypt('jason'),
            'image' => 'user.png',
            'gender' => 'male',
            'dob' => '09/11/1997',
            'address' => 'jalan bersama',
            'admin' => '0'
        ]);

        DB::table('phones')->insert([
            'image' => 'apple.png',
            'name' => 'Iphone XS',
            'brand' => 'Apple',
            'desc' => 'Get the most powerful Iphone ever in your hand right now! Here is the Iphone XS with 256GB internal memory and 4 GB of RAM that will satisfy you everytime and anywhere!',
            'price' => '23000000',
            'discount' => '10',
            'stock' => 10,
        ]);

        DB::table('phones')->insert([
            'image' => 'apple2.png',
            'name' => 'Iphone X',
            'brand' => 'Apple',
            'desc' => 'Get the most powerful Iphone ever in your hand right now! Here is the Iphone X with 128GB internal memory and 3 GB of RAM that will satisfy you everytime and anywhere!',
            'price' => '18000000',
            'discount' => '10',
            'stock' => 10,
        ]);

        DB::table('phones')->insert([
            'image' => 'apple3.png',
            'name' => 'Iphone 8',
            'brand' => 'Apple',
            'desc' => 'Get the most powerful Iphone ever in your hand right now! Here is the Iphone 8 with 64GB internal memory and 2 GB of RAM that will satisfy you everytime and anywhere!',
            'price' => '12000000',
            'discount' => '10',
            'stock' => 10,
        ]);

        DB::table('phones')->insert([
            'image' => 'apple4.png',
            'name' => 'Iphone 7',
            'brand' => 'Apple',
            'desc' => 'Get the most powerful Iphone ever in your hand right now! Here is the Iphone 7 with 32GB internal memory and 2 GB of RAM that will satisfy you everytime and anywhere!',
            'price' => '10000000',
            'discount' => '10',
            'stock' => 10,
        ]);

        DB::table('phones')->insert([
            'image' => 'apple5.png',
            'name' => 'Iphone 5s',
            'brand' => 'Apple',
            'desc' => 'Get the most powerful Iphone ever in your hand right now! Here is the Iphone 5s with 16GB internal memory and 1 GB of RAM that will satisfy you everytime and anywhere!',
            'price' => '4000000',
            'discount' => '10',
            'stock' => 10,
        ]);

        DB::table('phones')->insert([
            'image' => 'samsung.png',
            'name' => 'Galaxy S9',
            'brand' => 'Samsung',
            'desc' => 'Get the most powerful Galaxy ever in your hand right now! Here is the Galaxy S9 with 128GB internal memory and 4 GB of RAM that will satisfy you everytime and anywhere!',
            'price' => '11500000',
            'discount' => '10',
            'stock' => 10,
        ]);

        DB::table('phones')->insert([
            'image' => 'samsung2.png',
            'name' => 'Galaxy S8',
            'brand' => 'Samsung',
            'desc' => 'Get the most powerful Galaxy ever in your hand right now! Here is the Galaxy S8 with 64GB internal memory and 4 GB of RAM that will satisfy you everytime and anywhere!',
            'price' => '9500000',
            'discount' => '10',
            'stock' => 10,
        ]);

        DB::table('phones')->insert([
            'image' => 'samsung3.png',
            'name' => 'Galaxy S7',
            'brand' => 'Samsung',
            'desc' => 'Get the most powerful Galaxy ever in your hand right now! Here is the Galaxy S7 with 32GB internal memory and 4 GB of RAM that will satisfy you everytime and anywhere!',
            'price' => '8000000',
            'discount' => '10',
            'stock' => 10,
        ]);

        DB::table('phones')->insert([
            'image' => 'samsung4.png',
            'name' => 'Galaxy A6',
            'brand' => 'Samsung',
            'desc' => 'Get the most powerful Galaxy ever in your hand right now! Here is the Galaxy A6 with 32GB internal memory and 3 GB of RAM that will satisfy you everytime and anywhere!',
            'price' => '6000000',
            'discount' => '10',
            'stock' => 10,
        ]);

        DB::table('phones')->insert([
            'image' => 'samsung5.png',
            'name' => 'Galaxy J7',
            'brand' => 'Samsung',
            'desc' => 'Get the most powerful Galaxy ever in your hand right now! Here is the Galaxy J7 with 16GB internal memory and 2 GB of RAM that will satisfy you everytime and anywhere!',
            'price' => '4000000',
            'discount' => '10',
            'stock' => 10,
        ]);

        DB::table('brands')->insert([
            'brand' => 'Apple',
        ]);

        DB::table('brands')->insert([
            'brand' => 'Samsung',
        ]);
    }
}
