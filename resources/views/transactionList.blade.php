<!DOCTYPE html>
<html>
<head>
    <title>superceLL</title>
    <link rel="stylesheet" href="{{asset('css/customStyle.css')}}"/>
</head>
<body>
<div id = "header">
    <nav>
        <ul class="menu">
            {{--<li class="logoMain"><a href="#">superceLL</a></li>--}}

            <li ><a href= "#">Phones</a>
                <ul class = "submenu">
                    <li><a href="{{url('/insertPhone')}}">Insert Phone</a></li>
                    <li><a href="{{url('/managePhone')}}">Manage Phone</a></li>
                </ul>
            </li>

            <li ><a href= "#">Brand</a>
                <ul class = "submenu">
                    <li><a href="{{url('/insertBrand')}}">Insert Brands</a></li>
                    <li><a href="{{url('/manageBrand')}}">Manage Brands</a></li>
                </ul>
            </li>

            <li ><a href= "#">Member</a>
                <ul class = "submenu">
                    <li><a href="{{url('/insertMember')}}">Insert Member</a></li>
                    <li><a href="{{url('/manageMember')}}">Manage Member</a></li>
                </ul>
            </li>

            <li ><a href="{{url('/transactionList')}}">Transaction List</a></li>
        </ul>
        <ul class="wrap">
            <li><a href="{{url('/logout')}}">Logout</a></li>
            @auth
                <li><a href="#">Hi, {{Auth::user()->name}}</a></li>
                <li class="date" style="padding-top: 11px; padding-right: 12px;"><?php echo date('D, d M Y'); ?></li>
            @endif
        </ul>
    </nav>
</div>

<div style="height: 470px;">
    <h1 style="text-align: center">Transaction List <img style="height: 25px; width: 25px;" src="{{asset('images/shopcart.png')}}"></h1>

    <table class="table1">

        <tr>
            <th>ID</th>
            <th>E-mail</th>
            <th>Date</th>
            <th>Status</th>
            <th></th>
            <th></th>
        </tr>

        @foreach($transactions as $transaction)
            <tr>
                <td>{{$transaction->id}}</td>
                <td>{{$transaction->email}}</td>
                <td>{{$transaction->created_at}}</td>
                @if($transaction->status == "Success")
                    <td style="color: forestgreen;">{{$transaction->status}}</td>
                @elseif($transaction->status == "Pending")
                    <td style="color: red;">{{$transaction->status}}</td>
                @endif
                <td>
                    <a href="/transactionListDetail/{{$transaction->id}}"><button class="updateButton" type="submit" name="detailButton">Detail</button></a>
                </td>
                <td>
                    <a href="/deleteTransaction/{{$transaction->id}}"><button class="deleteButton" type="submit" name="deleteButton">Delete</button></a>
                </td>
            </tr>
        @endforeach
    </table>
</div>

<div id="footer">
    <p>
        superceLL©2018 | Your Daily Dose | Follow Us |
        <a href="http://www.facebook.com" target="_blank"><img src="{{asset('images/facebook.png')}}"></a>
        <a href="http://plus.google.com" target="_blank"><img src="{{asset('images/google-plus.png')}}"></a>
        <a href="http://www.twitter.com" target="_blank"><img src="{{asset('images/twitter.png')}}"></a>
        <a href="http://www.instagram.com" target="_blank"><img src="{{asset('images/instagram.png')}}"></a>
    </p>
</div>

</body>
</html>