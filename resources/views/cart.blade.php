<!DOCTYPE html>
<html>
<head>
    <title>superceLL</title>
    <link rel="stylesheet" href="{{asset('css/customStyle.css')}}" type="text/css" />
</head>
<body>
<div id = "header">
    <nav>
        <ul>
            {{--<li class="logoMain"><a href="#">superceLL</a></li>--}}
            @auth
                <li class="logo"><a href="/edit/{{Auth::user()->id}}">Update Profile</a></li>
            @endif
            <li class="logo"><a href="{{url('/phoneList')}}">Phone List</a></li>
            <li class="logo"><a href="/cart/{{Auth::user()->id}}">Cart <img class= "cart" src="{{asset('images/cart.png')}}"></a></li>
            <li class="logo"><a href="/transactionHistory/{{Auth::user()->id}}">Transaction History</a></li>

            <li><a href="{{url('/logout')}}">Logout</a></li>
            @auth
                <li><a href="#">Hi, {{Auth::user()->name}}</a></li>
                <li class="date" style="padding-top: 11px; padding-right: 12px;"><?php echo date('D, d M Y'); ?></li>
            @endif
        </ul>
    </nav>
</div>

<div style="height: 470px;">
    <h1 style="text-align: center">Your Cart <img style="height: 25px; width: 25px;" src="{{asset('images/shopcart.png')}}"></h1>

    @if($totalqty == 0)
        <h1 style="text-align: center; margin-top: 15%">You have no items!</h1>
    @else
    <div style="height: 500px;">
        <table class="table1">
            <tr>
                <th></th>
                <th>Phone</th>
                <th>Qty</th>
                <th>Price</th>
                <th>SubTotal</th>
                <th></th>
            </tr>

            @foreach($doBuy as $x)
                <tr>
                    <td><img id="commerce" src="{{asset('images/'.$x->image)}}"></td>
                    <td>{{$x->name}}</td>
                    <td>{{$x->qty}}</td>
                    <td>{{'Rp. '.($x->price - ($x->price * ($x->discount/100)))}}</td>
                    <td>{{'Rp. '.($x->price - ($x->price * ($x->discount/100))) * ($x->qty)}}</td>
                    <td>
                        <a href="/deleteCart/{{$x->id}}"><button class="deleteButton" id="deleteCart" type="submit" name="deleteCart">Delete</button></a>
                    </td>
                    <td></td>
                </tr>
            @endforeach

            <tr>
                <td></td>
                <td></td>
                <td>Total Qty: {{$totalqty}}</td>
                <td>Grand Total: {{$totalprice}}</td>
                <td>Input Payment: {{$totalprice}}</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><a href="/payment/{{Auth::user()->id}}"><button id="payButton">Complete the Payment</button></a> </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

        </table>
    </div>
    @endif
</div>

<div id="footer">
    <p>
        superceLL©2018 | Your Daily Dose | Follow Us |
        <a href="http://www.facebook.com" target="_blank"><img src="{{asset('images/facebook.png')}}"></a>
        <a href="http://plus.google.com" target="_blank"><img src="{{asset('images/google-plus.png')}}"></a>
        <a href="http://www.twitter.com" target="_blank"><img src="{{asset('images/twitter.png')}}"></a>
        <a href="http://www.instagram.com" target="_blank"><img src="{{asset('images/instagram.png')}}"></a>
    </p>
</div>

</body>
</html>