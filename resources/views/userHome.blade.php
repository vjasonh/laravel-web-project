<!DOCTYPE html>
<html>
<head>
	<title>superceLL</title>
	<link rel="stylesheet" href="{{asset('css/customStyle.css')}}" type="text/css" />
</head>
<body>
	<div id = "header">
		<nav>
			<ul>
				{{--<li class="logoMain"><a href="#">superceLL</a></li>--}}
				@auth
					<li class="logo"><a href="/edit/{{Auth::user()->id}}">Update Profile</a></li>
				@endif
				<li class="logo"><a href="{{url('/phoneList')}}">Phone List</a></li>
				<li class="logo"><a href="/cart/{{Auth::user()->id}}">Cart <img class= "cart" src="{{asset('images/cart.png')}}"></a></li>
				<li class="logo"><a href="/transactionHistory/{{Auth::user()->id}}">Transaction History</a></li>
				
				<li><a href="{{url('/logout')}}">Logout</a></li>
				@auth
					<li><a href="#">Hi, {{Auth::user()->name}}</a></li>
					<li class="date" style="padding-top: 11px; padding-right: 12px;"><?php echo date('D, d M Y'); ?></li>
				@endif
			</ul>
		</nav>
	</div>

	<div id = "main">
		
	</div>

	<div id="footer">
		<p>
			superceLL©2018 | Your Daily Dose | Follow Us | 
			<a href="http://www.facebook.com" target="_blank"><img src="{{asset('images/facebook.png')}}"></a>
	  		<a href="http://plus.google.com" target="_blank"><img src="{{asset('images/google-plus.png')}}"></a> 
	  		<a href="http://www.twitter.com" target="_blank"><img src="{{asset('images/twitter.png')}}"></a>
	  		<a href="http://www.instagram.com" target="_blank"><img src="{{asset('images/instagram.png')}}"></a>
		</p>
	</div>

</body>
</html>