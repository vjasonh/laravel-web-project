<!DOCTYPE html>
<html>
<head>
	<title>superceLL</title>
	<link rel="stylesheet" href="{{asset('css/customStyle.css')}}"/>
</head>
<body>
<div id = "header">
	<nav>
		<ul class="menu">
			{{--<li class="logoMain"><a href="#">superceLL</a></li>--}}

			<li ><a href= "#">Phones</a>
				<ul class = "submenu">
					<li><a href="{{url('/insertPhone')}}">Insert Phone</a></li>
					<li><a href="{{url('/managePhone')}}">Manage Phone</a></li>
				</ul>
			</li>

			<li ><a href= "#">Brand</a>
				<ul class = "submenu">
					<li><a href="{{url('/insertBrand')}}">Insert Brands</a></li>
					<li><a href="{{url('/manageBrand')}}">Manage Brands</a></li>
				</ul>
			</li>

			<li ><a href= "#">Member</a>
				<ul class = "submenu">
					<li><a href="{{url('/insertMember')}}">Insert Member</a></li>
					<li><a href="{{url('/manageMember')}}">Manage Member</a></li>
				</ul>
			</li>

			<li ><a href="{{url('/transactionList')}}">Transaction List</a></li>
		</ul>
		<ul class="wrap">
			<li><a href="{{url('/logout')}}">Logout</a></li>
			@auth
				<li><a href="#">Hi, {{Auth::user()->name}}</a></li>
				<li class="date" style="padding-top: 11px; padding-right: 12px;"><?php echo date('D, d M Y'); ?></li>
			@endif
		</ul>
	</nav>
</div>

<div style="background-color: #9d9d9d; height: 1000px; margin-top: -7px;">

	<div class="register-box">
		<h1> Insert Phone </h1>
		<form class="login" action="addPhone" method="POST" enctype="multipart/form-data">
			{{csrf_field()}}

			<div class="error">
				@if ($errors->has('image'))
					{{ $errors->first('image') }}
				@endif
				<br>
					<strong class="no-error">Image: </strong>
				<input class="no-error" type="file" name="image" id="image"><br>
			</div>

			<div class="error">
				@if ($errors->has('name'))
					{{ $errors->first('name') }}
				@endif
				<br>
				<input type="text" name="name" placeholder="Name">
				{{--<div class="username">--}}
				{{--<img src="{{asset('images/user.png')}}">--}}
				{{--</div>--}}
			</div>

			<div class="error">
				@if ($errors->has('brand'))
					{{ $errors->first('brand') }}
				@endif
				<br>
				<select class="no-error" name="brand">
					<option disabled selected value>=Choose Brand=</option>
					<option value="Apple">Apple</option>
					<option value="Samsung">Samsung</option>
					<option value="LG">LG</option>
				</select>
			</div>

			<div class="error">
				@if ($errors->has('desc'))
					{{ $errors->first('desc') }}
				@endif
				<br>
				<input type="text" name="desc" placeholder="Description">
				{{--<div class="email">--}}
				{{--<img src="{{asset('images/email.png')}}">--}}
				{{--</div>--}}
			</div>

			<div class="error">
				@if ($errors->has('price'))
					{{ $errors->first('price') }}
				@endif
				<br>
				<input type="text" name="price" placeholder="Price">
				{{--<div class="password">--}}
				{{--<img src="{{asset('images/password.png')}}">--}}
				{{--</div>--}}
			</div>
			<div class="error">
				@if ($errors->has('discount'))
					{{ $errors->first('discount') }}
				@endif
				<br>
				<input type="text" name="discount" placeholder="Discount">
				{{--<div class="passwordConfirm">--}}
				{{--<img src="{{asset('images/password.png')}}">--}}
				{{--</div>--}}
			</div>

			<div class="error">
				@if ($errors->has('stock'))
					{{ $errors->first('stock') }}
				@endif
				<br>
				<input type="text" name="stock" placeholder="Stock">
				{{--<div class="dob">--}}
				{{--<img src="{{asset('images/dob.png')}}">--}}
				{{--</div>--}}
			</div>

			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<input type="submit" name="insertPhone" value="Insert Phone">
		</form>
	</div>
</div>

<div id="footer">
	<p>
		superceLL©2018 | Your Daily Dose | Follow Us |
		<a href="http://www.facebook.com" target="_blank"><img src="{{asset('images/facebook.png')}}"></a>
		<a href="http://plus.google.com" target="_blank"><img src="{{asset('images/google-plus.png')}}"></a>
		<a href="http://www.twitter.com" target="_blank"><img src="{{asset('images/twitter.png')}}"></a>
		<a href="http://www.instagram.com" target="_blank"><img src="{{asset('images/instagram.png')}}"></a>
	</p>
</div>

</body>
</html>