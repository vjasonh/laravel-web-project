<!DOCTYPE html>
<html>
<head>
	<title>superceLL</title>
	<link rel="stylesheet" href="{{asset('css/customStyle.css')}}"/>
</head>
<body>
<div id = "header">
	<nav>
		<ul class="menu">
			{{--<li class="logoMain"><a href="#">superceLL</a></li>--}}

			<li ><a href= "#">Phones</a>
				<ul class = "submenu">
					<li><a href="{{url('/insertPhone')}}">Insert Phone</a></li>
					<li><a href="{{url('/managePhone')}}">Manage Phone</a></li>
				</ul>
			</li>

			<li ><a href= "#">Brand</a>
				<ul class = "submenu">
					<li><a href="{{url('/insertBrand')}}">Insert Brands</a></li>
					<li><a href="{{url('/manageBrand')}}">Manage Brands</a></li>
				</ul>
			</li>

			<li ><a href= "#">Member</a>
				<ul class = "submenu">
					<li><a href="{{url('/insertMember')}}">Insert Member</a></li>
					<li><a href="{{url('/manageMember')}}">Manage Member</a></li>
				</ul>
			</li>

			<li ><a href="#">Transaction List</a></li>
		</ul>
		<ul class="wrap">
			<li><a href="{{url('/logout')}}">Logout</a></li>
			@auth
				<li><a href="#">Hi, {{Auth::user()->name}}</a></li>
				<li class="date" style="padding-top: 11px; padding-right: 12px;"><?php echo date('D, d M Y'); ?></li>
			@endif
		</ul>
	</nav>
</div>

<div id = "main">
	<div id="login-form2">
		<h1> Update Brand </h1>
		<form class="login" action="/updateBrand" method="POST">
			{{csrf_field()}}

			<input type="hidden" name="id" value="{{$id}}">

			<div class="input-box2">
				<input type="text" name="brand" placeholder="Brand" value="{{$brand->brand}}">
				<br>
				<input type="submit" name="updateBrand" value="Update Brand">
				@if ($errors->has('brand'))
					<div class="error" style="text-align: center">{{ $errors->first('brand') }}</div>
				@endif
			</div>
		</form>
	</div>
</div>

<div id="footer">
	<p>
		superceLL©2018 | Your Daily Dose | Follow Us |
		<a href="http://www.facebook.com" target="_blank"><img src="{{asset('images/facebook.png')}}"></a>
		<a href="http://plus.google.com" target="_blank"><img src="{{asset('images/google-plus.png')}}"></a>
		<a href="http://www.twitter.com" target="_blank"><img src="{{asset('images/twitter.png')}}"></a>
		<a href="http://www.instagram.com" target="_blank"><img src="{{asset('images/instagram.png')}}"></a>
	</p>
</div>

</body>
</html>