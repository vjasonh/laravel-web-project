<!DOCTYPE html>
<html>
<head>
    <title>superceLL</title>
    <link rel="stylesheet" href="{{asset('css/customStyle.css')}}" type="text/css" />
</head>
<body>
<div id = "header">
    <nav>
        <ul>
            {{--<li class="logoMain"><a href="#">superceLL</a></li>--}}
            @auth
                <li class="logo"><a href="/edit/{{Auth::user()->id}}">Update Profile</a></li>
            @endif
            <li class="logo"><a href="{{url('/phoneList')}}">Phone List</a></li>
            <li class="logo"><a href="/cart/{{Auth::user()->id}}">Cart <img class= "cart" src="{{asset('images/cart.png')}}"></a></li>
            <li class="logo"><a href="/transactionHistory/{{Auth::user()->id}}">Transaction History</a></li>

            <li><a href="{{url('/logout')}}">Logout</a></li>
            @auth
                <li><a href="#">Hi, {{Auth::user()->name}}</a></li>
                <li class="date" style="padding-top: 11px; padding-right: 12px;"><?php echo date('D, d M Y'); ?></li>
            @endif
        </ul>
    </nav>
</div>

<section class="content">

    <form method="post" action="/addingToCart">
        {{csrf_field()}}

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <input type="hidden" value="{{$id}}" name="phone_id">
    <input type="hidden" value="{{Auth::user()->id}}" name="user_id">

    <h1>{{$product->name}}</h1>
    <div><img src="{{asset('images/'. $product->image)}}"></div>
    <div>
        <h2>Brand: {{$product->brand}}<br><br>
            Special Price:<br>
            <strike>{{'Rp. '.($product->price - ($product->price * ($product->discount/100)))}}</strike><br>
        </h2>
        <div style="font-size: 35px">{{'Rp. '. $product->price}}</div><br>
        <h2>Stock: {{$product->stock}}<br></h2>
    </div>
    <p>
    <br>
        <h2>Description:</h2>
        <h3>{{$product->desc}}</h3>
    </p>

    <div style="display: inline-flex">
        <h3>Quantity: </h3>
        <input type="number" style="font-size: x-large; text-align: center; margin-left: 30px; margin-bottom: 25px; height: 40px; width: 50px;" name="qty" max="{{$product->stock}}" value="1">
    </div>

        <br>

    <button id="addCart" type="submit" name="addCart">Add to Cart <img class= "cart" src="{{asset('images/cart.png')}}"></button>
    </form>

    <h2>Comments:</h2>
    <div id="scrollpane">
        <table class="table1">
            @foreach($comments as $comment)
                <tr>
                    <td>
                    <strong>{{$comment->email}}</strong><br>
                        {{$comment->comment}}
                    </td>
                    <td>
                        {{$comment->created_at}}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    <div id="comment-box">
    <form action="/comment/{{$id}}" method="post">
        {{@csrf_field()}}
        <input type="hidden" value="{{$id}}" name="phone_id">
        <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
        <div>
            <h2>Insert Comment Here:</h2>
        </div>
        <div style="width: auto">
            <input type="text" name="comment" placeholder="Type Your Comment Here!">
        </div>
        <div>
            <input type="submit" value="Insert Comment">
        </div>
    </form>
    </div>
</section>

<div id="footer">
    <p>
        superceLL©2018 | Your Daily Dose | Follow Us |
        <a href="http://www.facebook.com" target="_blank"><img src="{{asset('images/facebook.png')}}"></a>
        <a href="http://plus.google.com" target="_blank"><img src="{{asset('images/google-plus.png')}}"></a>
        <a href="http://www.twitter.com" target="_blank"><img src="{{asset('images/twitter.png')}}"></a>
        <a href="http://www.instagram.com" target="_blank"><img src="{{asset('images/instagram.png')}}"></a>
    </p>
</div>

</body>
</html>