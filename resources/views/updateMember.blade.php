<!DOCTYPE html>
<html>
<head>
	<title>superceLL</title>
	<link rel="stylesheet" href="{{asset('css/customStyle.css')}}"/>
</head>
<body>
<div id = "header">
	<nav>
		<ul class="menu">
			{{--<li class="logoMain"><a href="#">superceLL</a></li>--}}

			<li ><a href= "#">Phones</a>
				<ul class = "submenu">
					<li><a href="{{url('/insertPhone')}}">Insert Phone</a></li>
					<li><a href="{{url('/managePhone')}}">Manage Phone</a></li>
				</ul>
			</li>

			<li ><a href= "#">Brand</a>
				<ul class = "submenu">
					<li><a href="{{url('/insertBrand')}}">Insert Brands</a></li>
					<li><a href="{{url('/manageBrand')}}">Manage Brands</a></li>
				</ul>
			</li>

			<li ><a href= "#">Member</a>
				<ul class = "submenu">
					<li><a href="{{url('/insertMember')}}">Insert Member</a></li>
					<li><a href="{{url('/manageMember')}}">Manage Member</a></li>
				</ul>
			</li>

			<li ><a href="#">Transaction List</a></li>
		</ul>
		<ul class="wrap">
			<li><a href="{{url('/logout')}}">Logout</a></li>
			@auth
				<li><a href="#">Hi, {{Auth::user()->name}}</a></li>
				<li class="date" style="padding-top: 11px; padding-right: 12px;"><?php echo date('D, d M Y'); ?></li>
			@endif
		</ul>
	</nav>
</div>

<div style="background-color: #9d9d9d; height: 1000px;  margin-top: -7px;">

	<div class="register-box">
		<h1> Update Member </h1>
		<form class="login" action="/updateMember" method="POST" enctype="multipart/form-data">
			{{csrf_field()}}

			<input type="hidden" name="id" value="{{$id}}">

			<strong>Current Profile Picture:</strong> <br>
			<img style="height: 100px; width: 100px" src="{{asset('images/'.$member->image)}}"> <br>
			<div>{{$member->image}}</div> <br>

			<div class="error">
				@if ($errors->has('name'))
					{{ $errors->first('name') }}
				@endif
				<br>
				<input type="text" name="name" placeholder="Name" value="{{$member->name}}">
				{{--<div class="username">--}}
				{{--<img src="{{asset('images/user.png')}}">--}}
				{{--</div>--}}
			</div>
			<div class="error">
				@if ($errors->has('email'))
					{{ $errors->first('email') }}
				@endif
				<br>
				<input type="text" name="email" placeholder="Email" value="{{$member->email}}">
				{{--<div class="email">--}}
				{{--<img src="{{asset('images/email.png')}}">--}}
				{{--</div>--}}
			</div>

			<div class="error">
				@if ($errors->has('image'))
					{{ $errors->first('image') }}
				@endif
				<br>
				<strong class="no-error">Profile Picture: </strong>
				<input class="no-error" type="file" name="image" id="image"><br>

			</div>

			<div class="error">
				@if ($errors->has('gender'))
					{{ $errors->first('gender') }}
				@endif
				<br>
					@if($member->gender == 'male')
						<div class="no-error">
							<strong class="no-error">Gender: </strong>
							<input class="no-error" type="radio" name="gender" value="male" checked> Male
							<input class="no-error" type="radio" name="gender" value="female"> Female
						</div>
					@elseif($member->gender == 'female')
						<div class="no-error">
							<strong class="no-error">Gender: </strong>
							<input class="no-error" type="radio" name="gender" value="male"> Male
							<input class="no-error" type="radio" name="gender" value="female" checked> Female
						</div>
					@else
						<div class="no-error">
							<strong class="no-error">Gender: </strong>
							<input class="no-error" type="radio" name="gender" value="male"> Male
							<input class="no-error" type="radio" name="gender" value="female"> Female
						</div>
					@endif
			</div>

			<br>

			<div class="error">
				@if ($errors->has('dob'))
					{{ $errors->first('dob') }}
				@endif
				<br>
				<input type="text" name="dob" placeholder="dd/mm/yyyy" value="{{$member->dob}}">
				{{--<div class="dob">--}}
				{{--<img src="{{asset('images/dob.png')}}">--}}
				{{--</div>--}}
			</div>

			<div class="error">
				@if ($errors->has('address'))
					{{ $errors->first('address') }}
				@endif
				<br>
				<input type="text" name="address" placeholder="Address" value="{{$member->address}}">
				{{--<div class="address">--}}
				{{--<img src="{{asset('images/address.png')}}">--}}
				{{--</div>--}}
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<input type="submit" name="updateMember" value="Update Member">
		</form>
	</div>
</div>

<div id="footer">
	<p>
		superceLL©2018 | Your Daily Dose | Follow Us |
		<a href="http://www.facebook.com" target="_blank"><img src="{{asset('images/facebook.png')}}"></a>
		<a href="http://plus.google.com" target="_blank"><img src="{{asset('images/google-plus.png')}}"></a>
		<a href="http://www.twitter.com" target="_blank"><img src="{{asset('images/twitter.png')}}"></a>
		<a href="http://www.instagram.com" target="_blank"><img src="{{asset('images/instagram.png')}}"></a>
	</p>
</div>

</body>
</html>