<!DOCTYPE html>
<html>
<head>
	<title>superceLL</title>
	<link rel="stylesheet" href="{{asset('css/customStyle.css')}}" type="text/css" />
</head>
<body>
	<div id = "header">
		<nav>
			<ul>
				{{--<li class="logoMain"><a href="#">superceLL</a></li>--}}
				<li><a style="color: lightskyblue" href="{{url('register')}}">Register</a></li>
				<li><a style="color: greenyellow" href="{{url('login')}}">Login</a></li>
				<li class="date" style="padding-top: 11px; padding-right: 12px;"><?php echo date('D, d M Y'); ?></li>
			</ul>
		</nav>
	</div>

	<div id = "main">
		<div id="login-form">
			<h1> LOGIN </h1>
			<form class="login" action="doLogin" method="POST">
				{{csrf_field()}}
				<div class="input-box">
					<input type="text" name="email" placeholder="Email" value={{Cookie::get('credentials') != null ? Cookie::get('credentials')['email']:""}}>
					<div class="user">
						<img src={{asset('images/user.png')}}>
					</div>
					<br>
					<input type="password" name="password" placeholder="Password" value={{Cookie::get('credentials') != null ? Cookie::get('credentials')['password']:""}}>
					<div class="password">
						<img src={{asset('images/password.png')}}>
					</div>
				</div>
				<br>
				<input type="checkbox" name="remember" value="Remember Me"> Remember Me
				<br>
				<input type="submit" name="login" value="Log in">
			</form>
			@if (session('alert'))
				<div style="color: red;" class="alert alert-success">
					{{ session('alert') }}
				</div>
			@endif
		</div>
	</div>

	<div id="footer">
		<p>
			superceLL©2018 | Your Daily Dose | Follow Us | 
			<a href="http://www.facebook.com" target="_blank"><img src="{{asset('images/facebook.png')}}"></a>
	  		<a href="http://plus.google.com" target="_blank"><img src="{{asset('images/google-plus.png')}}"></a> 
	  		<a href="http://www.twitter.com" target="_blank"><img src="{{asset('images/twitter.png')}}"></a>
	  		<a href="http://www.instagram.com" target="_blank"><img src="{{asset('images/instagram.png')}}"></a>
		</p>
	</div>

</body>
</html>