<!DOCTYPE html>
<html>
<head>
    <title>superceLL</title>
    <link rel="stylesheet" href="{{asset('css/customStyle.css')}}" type="text/css" />
</head>
<body>
<div id = "header">
    <nav>
        <ul>
            {{--<li class="logoMain"><a href="#">superceLL</a></li>--}}
            @auth
                <li class="logo"><a href="/edit/{{Auth::user()->id}}">Update Profile</a></li>
            @endif
            <li class="logo"><a href="{{url('/phoneList')}}">Phone List</a></li>
            <li class="logo"><a href="/cart/{{Auth::user()->id}}">Cart <img class= "cart" src="{{asset('images/cart.png')}}"></a></li>
            <li class="logo"><a href="/transactionHistory/{{Auth::user()->id}}">Transaction History</a></li>

            <li><a href="{{url('/logout')}}">Logout</a></li>
            @auth
                <li><a href="#">Hi, {{Auth::user()->name}}</a></li>
                <li class="date" style="padding-top: 11px; padding-right: 12px;"><?php echo date('D, d M Y'); ?></li>
            @endif
        </ul>
    </nav>
</div>

<div style="height: auto;">
    <strong style="font-size: xx-large; margin-left: 42%;">Phone List</strong>
    <table class="table1">
        <tr>
            <form action="/search">
                <div id="group">
                    <input id="search-bar" type="text" name="searchbar" placeholder="Search by Name / Brand">
                    <button id="search-button" type="submit" name="search">Search</button>
                    <select id="search-option" name="searchoption">
                        <option value="name">Name</option>
                        <option value="brand">Brand</option>
                    </select>
                </div>
            </form>
        </tr>

        <tr>
            <th>ID</th>
            <th></th>
            <th>Phone</th>
            <th>Normal Price</th>
            <th>Discount</th>
            <th>Special Price</th>
            <th>Add to Cart</th>
        </tr>

        @foreach($products as $product)
        <tr>
            <td>{{$product->id}}</td>
            <td><img id="commerce" src="{{asset('images/'.$product->image)}}"></td>
            <td>{{$product->name}}</td>
            <td><strike>{{'Rp. '.$product->price}}</strike></td>
            <td>{{$product->discount.'%'}}</td>
            <td>{{'Rp. '.($product->price - ($product->price * ($product->discount/100)))}}</td>
            <td>
                <a href="/addCart/{{$product->id}}"><button id="buyButton" type="submit" name="buy">Buy</button></a>
            </td>
        </tr>
        @endforeach
    </table>
    {{$products->links()}}
</div>


<div id="footer">
    <p>
        superceLL©2018 | Your Daily Dose | Follow Us |
        <a href="http://www.facebook.com" target="_blank"><img src="{{asset('images/facebook.png')}}"></a>
        <a href="http://plus.google.com" target="_blank"><img src="{{asset('images/google-plus.png')}}"></a>
        <a href="http://www.twitter.com" target="_blank"><img src="{{asset('images/twitter.png')}}"></a>
        <a href="http://www.instagram.com" target="_blank"><img src="{{asset('images/instagram.png')}}"></a>
    </p>
</div>

</body>
</html>