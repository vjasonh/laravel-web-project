<!DOCTYPE html>
<html>
<head>
    <title>superceLL</title>
    <link rel="stylesheet" href="{{asset('css/customStyle.css')}}"/>
</head>
<body>
<div id = "header">
    <nav>
        <ul class="menu">
            {{--<li class="logoMain"><a href="#">superceLL</a></li>--}}

            <li ><a href= "#">Phones</a>
                <ul class = "submenu">
                    <li><a href="{{url('/insertPhone')}}">Insert Phone</a></li>
                    <li><a href="{{url('/managePhone')}}">Manage Phone</a></li>
                </ul>
            </li>

            <li ><a href= "#">Brand</a>
                <ul class = "submenu">
                    <li><a href="{{url('/insertBrand')}}">Insert Brands</a></li>
                    <li><a href="{{url('/manageBrand')}}">Manage Brands</a></li>
                </ul>
            </li>

            <li ><a href= "#">Member</a>
                <ul class = "submenu">
                    <li><a href="{{url('/insertMember')}}">Insert Member</a></li>
                    <li><a href="{{url('/manageMember')}}">Manage Member</a></li>
                </ul>
            </li>

            <li ><a href="{{url('/transactionList')}}">Transaction List</a></li>
        </ul>
        <ul class="wrap">
            <li><a href="{{url('/logout')}}">Logout</a></li>
            @auth
                <li><a href="#">Hi, {{Auth::user()->name}}</a></li>
                <li class="date" style="padding-top: 11px; padding-right: 12px;"><?php echo date('D, d M Y'); ?></li>
            @endif
        </ul>
    </nav>
</div>

<div style="height: 470px; margin-top: 20px">
    <strong style="font-size: xx-large; margin-left: 42%;">Manage Members</strong>
    <table class="table1">
        <tr>
            <th>ID</th>
            <th>Member Name</th>
            <th></th>
            <th></th>
        </tr>

        @foreach($members as $member)
        <tr>
            <th>{{$member->id}}</th>
            <th>{{$member->name}}</th>
            <th>
                <a href="/editMember/{{$member->id}}"><button class="updateButton">Update</button></a>
            </th>
            <th>
                <a href="/deleteMember/{{$member->id}}"><button class="deleteButton">Delete</button></a>
            </th>
        </tr>
        @endforeach
    </table>
</div>

<div id="footer">
    <p>
        superceLL©2018 | Your Daily Dose | Follow Us |
        <a href="http://www.facebook.com" target="_blank"><img src="{{asset('images/facebook.png')}}"></a>
        <a href="http://plus.google.com" target="_blank"><img src="{{asset('images/google-plus.png')}}"></a>
        <a href="http://www.twitter.com" target="_blank"><img src="{{asset('images/twitter.png')}}"></a>
        <a href="http://www.instagram.com" target="_blank"><img src="{{asset('images/instagram.png')}}"></a>
    </p>
</div>

</body>
</html>