<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Phone;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class transactionController extends Controller
{
    public function addComment(Request $request){
        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->phone_id = $request->phone_id;
        $comment->user_id = $request->user_id;
        $comment->save();

        return redirect('/addCart/'. $request->phone_id);
    }

    public function viewCart($id){

        $doBuy = DB::table('transactions')->join('phones','phones.id','=','transactions.phone_id')->join('users','users.id','=','transactions.user_id')->select('phones.image','phones.name','phones.price','phones.discount','transactions.*')->where('transactions.user_id','=',$id)->where('transactions.status','=','Pending')->get();

        $totalqty = DB::table('transactions')->where('transactions.user_id','=',$id)->where('transactions.status','=','Pending')->sum('qty');

        $totalprice = DB::table('transactions')->join('phones','phones.id','=','transactions.phone_id')->join('users','users.id','=','transactions.user_id')->where('transactions.user_id','=',$id)->where('transactions.status','=','Pending')->sum(DB::raw('(phones.price - (phones.price * phones.discount / 100)) * transactions.qty'));

        return(view('/cart',compact('doBuy','totalqty','totalprice')));
    }

    public function payItem($id){
            DB::table('transactions')->where('transactions.user_id','=',$id)->where('transactions.status','=','Pending')->update(['status' => 'Success' ]);

        return redirect('user');
    }

    public function addCart($id){

        $product = Phone::find($id);

        $comments   = DB::table('comments')->join('phones','phones.id','=','comments.phone_id')->join('users','users.id','=','comments.user_id')->select('comments.*','users.email')->where('comments.phone_id','=', $id)->get();

        return view('addCart', compact('id','product', 'comments'));
    }

    public function addingCart(Request $request){

        $transaction = new Transaction();
        $transaction->user_id = $request->user_id;
        $transaction->phone_id = $request->phone_id;
        $transaction->qty = $request->qty;
        $transaction->status = 'Pending';
        $transaction->save();

        return redirect('user');
    }

    public function deleteCart($id){

        $transaction = Transaction::find($id);
        $transaction->delete();

        return redirect('user');
    }

    public function showTransaction($id){
        $transactions = DB::table('transactions')->join('users','users.id','=','transactions.user_id')->select('transactions.id','transactions.created_at','transactions.status','users.email')->where('transactions.user_id','=',$id)->get();

        return(view('transactionHistory',compact('transactions')));
    }

    public function transactionDetail($id){
        $transactionid = DB::table('transactions')->join('phones','phones.id','=','transactions.phone_id')->select('phones.name','phones.price','phones.discount','transactions.qty')->where('transactions.id','=',$id)->first();

        $totalqty = DB::table('transactions')->where('transactions.id','=',$id)->sum('qty');

        $totalprice = DB::table('transactions')->join('phones','phones.id','=','transactions.phone_id')->join('users','users.id','=','transactions.user_id')->where('transactions.id','=',$id)->sum(DB::raw('(phones.price -( phones.price * phones.discount / 100) ) * transactions.qty'));

        return(view('transactionDetail',compact('transactionid','totalprice','totalqty')));
    }

    public function transactionList(){
        $transactions   = DB::table('transactions')->join('users','users.id','=','transactions.user_id')->select('transactions.id','transactions.created_at','transactions.status','users.email')->get();

        return(view('transactionList',compact('transactions')));
    }

    public function transactionListDetail($id){
        $transactionid = DB::table('transactions')->join('phones','phones.id','=','transactions.phone_id')->select('phones.name','phones.price','phones.discount','transactions.qty')->where('transactions.id','=',$id)->first();

        $totalqty = DB::table('transactions')->where('transactions.id','=',$id)->sum('qty');

        $totalprice = DB::table('transactions')->join('phones','phones.id','=','transactions.phone_id')->join('users','users.id','=','transactions.user_id')->where('transactions.id','=',$id)->sum(DB::raw('(phones.price -( phones.price * phones.discount / 100) ) * transactions.qty'));

        return(view('transactionListDetail',compact('transactionid','totalprice','totalqty')));
    }

    public function deleteTransaction($id){
        $transaction = Transaction::find($id);
        $transaction->delete();

        return redirect('/transactionList');
    }

    public function cart(){
        return view('cart');
    }
}