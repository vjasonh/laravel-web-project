<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class loginController extends Controller
{
    public function login(){
    	return view('login');



  //   	$emailErr = "";
  //   	if(isset($_POST['email']) == true && empty($_POST['email']) == false){
  //   		$email = $_POST['email'];
	 //    	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
		// 	  	echo ("valid!");
		// 	} else {
		// 		$emailErr = "email is not a valid email address"; 
		// 	  	return view('login');
		// 	}
		// }
  //   	else{
  //   		return view('login');
  //   	}
    	//return view('login');
    }

    public function doLogin(Request $request){

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
            'admin' => '0',
        ];

        $credentialsAdmin = [
            'email' => $request->email,
            'password' => $request->password,
            'admin' => '1',
        ];

        if ($request->remember) {
            Cookie::queue('credentials', $credentials, 60); //cookie kesimpen 1 jam
        } else if($request->remember == null){
            Cookie::queue(cookie()->forget('email'));
            Cookie::queue(cookie()->forget('password'));
        }

        if (Auth::attempt($credentials)){
            return redirect('/user');
        } else if(Auth::attempt($credentialsAdmin)){
            return redirect('/admin');
        } else {
            return redirect()->back()->with('alert', 'Username and Password do not exist!');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('/login');
    }

    public function user(){
    	return view('userHome');
    }

    public function admin(){
        return view('adminHome');
    }

    public function index(){
        return view('index');
    }
}
