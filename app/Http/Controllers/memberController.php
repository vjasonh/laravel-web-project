<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class memberController extends Controller
{
    public function addingMember(){
        return view('insertMember');
    }

    public function addMember(Request $request){
        $rules = [
            'name' => 'required | min:3',
            'email' => 'required | email | unique:users',
            'password' => 'required | alpha_num | min:5',
            'passwordConfirm' => 'required | same:password',
            'image' => 'required',
            'gender' => 'required|in:male,female',
            'dob' => 'required | date_format:d/m/Y',
            'address' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return redirect('/insertMember')->withErrors($validator);
        }

        $filename = $request->file('image')->getClientOriginalName();
        Storage::putFileAs('public/pictures', $request->file('image'), $filename);

        $members = new User();
        $members->name = $request->name;
        $members->email = $request->email;
        $members->password = bcrypt($request->password);
        $members->image = $filename;
        $members->gender = $request->gender;
        $members->dob = $request->dob;
        $members->address = $request->address;
        $members->save();

        return redirect('admin');
    }

    public function manageMember(){
        $members = User::all();

        return view('manageMember', ['members' => $members]);
    }

    public function editMember($id)
    {
        $member = User::find($id);
        return view('updateMember', compact('id', 'member'));
    }

    public function updateMember(Request $request)
    {
        $rules = [
            'name' => 'required | min:3',
            'email' => 'required | email | unique:users',
            'image' => 'required',
            'gender' => 'required | in:male,female',
            'dob' => 'required | date_format:d/m/Y',
            'address' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return redirect('/editMember/'. $request->id)->withErrors($validator);
        }

        $members = User::find($request->id);

        $filename = $request->file('image')->getClientOriginalName();
        Storage::putFileAs('public/pictures', $request->file('image'), $filename);

        $members->name = $request->name;
        $members->email = $request->email;
        $members->image = $filename;
        $members->gender = $request->gender;
        $members->dob = $request->dob;
        $members->address = $request->address;
        $members->save();

        return redirect('manageMember');
    }

    public function deleteMember($id){
        $member = User::find($id);
        $member->delete();
        return redirect('manageMember');
    }
}
