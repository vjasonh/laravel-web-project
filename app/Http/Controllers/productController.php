<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Phone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class productController extends Controller
{
    public function phoneList(){
        $products = Phone::paginate(8);

        return view('phoneList', ['products' => $products]);
    }

    public function search(Request $request){
        if($request->searchoption == 'name') {

            $input = $request->searchbar;
            $products = Phone::where('name', 'like', "%$input%")->paginate(8);

            return view('phoneList', compact('products'));
        } else {

            $input = $request->searchbar;
            $products = Phone::where('brand', 'like', "%$input%")->paginate(8);

            return view('phoneList', compact('products'));
        }
    }

    public function addingPhone(){
        return view('insertPhone');
    }

    public function addPhone(Request $request)
    {
        $rules = [
            'image' => 'required',
            'name' => 'required | min:3 | unique:phones',
            'brand' => 'required',
            'desc' => 'required',
            'price' => 'required | numeric | min:1000',
            'discount' => 'required',
            'stock' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return redirect('/insertPhone')->withErrors($validator);
        }

        $filename = $request->file('image')->getClientOriginalName();
        Storage::putFileAs('public/pictures', $request->file('image'), $filename);

        $products = new Phone();
        $products->image = $filename;
        $products->name = $request->name;
        $products->brand = $request->brand;
        $products->desc = $request->desc;
        $products->price = $request->price;
        $products->discount = $request->discount;
        $products->stock = $request->stock;
        $products->save();

        return redirect('admin');
    }

    public function managePhone(){
        $products = Phone::all();

        return view('managePhone', ['products' => $products]);
    }

    public function adminSearch(Request $request){
        if($request->searchoption == 'name') {
            $input = $request->searchbar;
            $products = Phone::where('name', 'like', "%$input%")->paginate(8);

            return view('managePhone', compact('products'));
        } else if($request->searchoption == 'brand'){
            $input = $request->searchbar;
            $products = Phone::where('brand', 'like', "%$input%")->paginate(8);

            return view('managePhone', compact('products'));
        }
    }

    public function editPhone($id)
    {
        $product = Phone::find($id);
        $brands = Brand::all();

        return view('updatePhone', compact('id', 'product', 'brands'));
    }

    public function updatePhone(Request $request)
    {
        $rules = [
            'image' => 'required',
            'name' => 'required | min:3 | unique:phones',
            'brand' => 'required',
            'desc' => 'required',
            'price' => 'required | numeric | min:1000',
            'discount' => 'required',
            'stock' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return redirect('/editPhone/'. $request->id)->withErrors($validator);
        }

        $products = Phone::find($request->id);

        $filename = $request->file('image')->getClientOriginalName();
        Storage::putFileAs('public/pictures', $request->file('image'), $filename);

        $products->image = $filename;
        $products->name = $request->name;
        $products->brand = $request->brand;
        $products->desc = $request->desc;
        $products->price = $request->price;
        $products->discount = $request->discount;
        $products->stock = $request->stock;
        $products->save();

        return redirect('managePhone');
    }

    public function deletePhone($id){
        $product = Phone::find($id);
        $product->delete();

        return redirect('managePhone');
    }

    public function addingBrand(){
        return view('insertBrand');
    }

    public function addBrand(Request $request){
        $rules = [
            'brand' => 'required | unique:brands'
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return redirect('/insertBrand')->withErrors($validator);
        }

        $brands = new Brand();
        $brands->brand = $request->brand;
        $brands->save();

        return redirect('admin');
    }

    public function manageBrand(){
        $brands = Brand::all();

        return view('manageBrand', ['brands' => $brands]);
    }

    public function editBrand($id)
    {
        $brand = Brand::find($id);

        return view('updateBrand', compact('id', 'brand'));
    }

    public function updateBrand(Request $request)
    {
        $rules = [
            'brand' => 'required | unique:brands'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return redirect('/editBrand/'. $request->id)->withErrors($validator);
        }

        $brands = Brand::find($request->id);

        $brands->brand = $request->brand;
        $brands->save();

        return redirect('manageBrand');
    }

    public function deleteBrand($id){
        $brand = Brand::find($id);
        $brand->delete();

        return redirect('manageBrand');
    }
}
