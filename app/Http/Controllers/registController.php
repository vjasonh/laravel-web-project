<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class registController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        return view('register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required | min:3',
            'email' => 'required | email | unique:users',
            'password' => 'required | min:5 | alpha_num | required_with:passwordConfirm',
            'passwordConfirm' => 'required | same:password',
            'image' => 'required',
            'gender' => 'required | in:male,female',
            'dob' => 'required | date_format:d/m/Y',
            'address' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return redirect('/register')->withErrors($validator);
        }

        $filename = $request->file('image')->getClientOriginalName();
        Storage::putFileAs('public/pictures', $request->file('image'), $filename);

        $data = new User();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->image = $filename;
        $data->gender = $request->gender;
        $data->dob = $request->dob;
        $data->address = $request->address;
        $data->save();

        return redirect('login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        return view('update', compact('id', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $rules = [
            'name' => 'required | min:3',
            'email' => 'required | email | unique:users',
            'image' => 'required',
            'gender' => 'required | in:male,female',
            'dob' => 'required | date_format:d/m/Y',
            'address' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return redirect('/edit/'. $request->id)->withErrors($validator);
        }

        $data = User::find($request->id);

        $filename = $request->file('image')->getClientOriginalName();
        Storage::putFileAs('public/pictures', $request->file('image'), $filename);

        $data->name = $request->name;
        $data->email = $request->email;
        $data->image = $filename;
        $data->gender = $request->gender;
        $data->dob = $request->dob;
        $data->address = $request->address;
        $data->save();

        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
