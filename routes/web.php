<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//LOGIN & REGISTER
//===================================================================================
Route::get('/', 'loginController@index');

Route::get('/login', 'loginController@login');
Route::post('/doLogin', 'loginController@doLogin');

Route::get('/register', 'registController@register');
Route::post('/store','registController@store');

Route::get('/logout', 'loginController@logout');

//Member
//===================================================================================
Route::get('/user', 'loginController@user');
Route::get('/edit/{id}', 'registController@edit');
Route::post('/updateProfile', 'registController@updateProfile');

Route::get('/phoneList', 'productController@phonelist');
Route::get('/search', 'productController@search');
Route::get('/addCart/{id}', 'transactionController@addCart');
Route::post('/comment/{id}', 'transactionController@addComment');
Route::post('/addingToCart', 'transactionController@addingCart');
Route::get('/cart/{id}', 'transactionController@viewCart');
Route::get('/payment/{id}', 'transactionController@payItem');
Route::get('/deleteCart/{id}', 'transactionController@deleteCart');

Route::get('/transactionHistory/{id}', 'transactionController@showTransaction');
Route::get('/transactionDetail/{id}', 'transactionController@transactionDetail');

//Admin
//===================================================================================
Route::middleware(['checkAdmin'])->group(function () {
    Route::get('/admin', 'loginController@admin');
    Route::get('/insertPhone', 'productController@addingPhone');
    Route::post('/addPhone', 'productController@addPhone');
    Route::get('/managePhone', 'productController@managePhone');
    Route::get('/adminSearch', 'productController@adminSearch');
    Route::get('/editPhone/{id}', 'productController@editPhone');
    Route::post('/updatePhone', 'productController@updatePhone');
    Route::get('/deletePhone/{id}', 'productController@deletePhone');

    Route::get('/insertBrand', 'productController@addingBrand');
    Route::post('/addBrand', 'productController@addBrand');
    Route::get('/manageBrand', 'productController@manageBrand');
    Route::get('/editBrand/{id}', 'productController@editBrand');
    Route::post('/updateBrand', 'productController@updateBrand');
    Route::get('/deleteBrand/{id}', 'productController@deleteBrand');

    Route::get('/insertMember', 'memberController@addingMember');
    Route::post('/addMember', 'memberController@addMember');
    Route::get('/manageMember', 'memberController@manageMember');
    Route::get('/editMember/{id}', 'memberController@editMember');
    Route::post('/updateMember', 'memberController@updateMember');
    Route::get('/deleteMember/{id}', 'memberController@deleteMember');

    Route::get('/transactionList', 'transactionController@transactionList');
    Route::get('/transactionListDetail/{id}', 'transactionController@transactionListDetail');
    Route::get('/deleteTransaction/{id}', 'transactionController@deleteTransaction');
});